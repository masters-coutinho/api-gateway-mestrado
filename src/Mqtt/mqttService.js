const mqtt = require('mqtt')
const lodash = require('lodash')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const Device = require('../Device/device')
const Token = require('../Token/token')
const env = require('../Config/constants')
const { send } = require('process')
var crypto = require('crypto');
var CryptoJS = require("crypto-js");
const { isValidObjectId } = require('mongoose')
let moment = require('moment')
const Dado = require('../DadoTeste/dado')
var fetchUrl = require("fetch").fetchUrl;


const keycript="70fd019e13f611ebaae874d4359afc5a"
const algoritmo="aes-256-ctr"

let criptografaDado = function(dado){

  var mykey = crypto.createCipher(algoritmo, keycript);
  let dadoCriptografado = mykey.update(dado, 'utf8', 'hex')
  dadoCriptografado += mykey.final('hex');
  return dadoCriptografado

}

Token.updateOptions({new:true, runValidators:true})

Token.after('post', sendErrorsOrNext).after('put',sendErrorsOrNext)

function sendErrorsOrNext(req,resp,next){
    const bundle = resp.locals.bundle

    if(bundle.errors){
        var errors = parseErrors(bundle.errors)
        resp.status(500).json({errors})
    }else{
        next()
    }
}

function parseErrors(nodeRestfulErrors){

    const errors = []
    lodash.forIn(nodeRestfulErrors, error =>{
        errors.push(error.message)
    })
}



// console.log(CryptoJS.HmacSHA1("Message", "Key"));



// var mykey = crypto.createCipher('aes-128-cbc', 'mypassword');
// var mystr = mykey.update('Fábio', 'utf8', 'hex')
// mystr += mykey.final('hex');

// console.log(mystr);

// var mykeyDec = crypto.createDecipher('aes-128-cbc', 'mypassword');
// var mystr2 = mykeyDec.update('8835072260e01b01871e64007ade29ad', 'hex', 'utf8')
// mystr2 += mykeyDec.final('utf8');

// console.log(mystr2); //abc

const topico_subscribe = "MQTTMestradoFabio"
const topico_subscribe_teste = "MQTTMestradoFabioTeste"
let tokenNuvem
const topico_subscribe_latencia = "MQTTTesteLatencia"


let sendNotification = function(mensagem,token) {
  let headers = {
    "Content-Type": "application/json;charset=utf-8",
    "Authorization": token
  };

  let jsonObject = JSON.parse(mensagem)

  let data = {
    codigo: jsonObject.Codigo,
    deviceId : jsonObject.DeviceId,
    timestampInicial : jsonObject.Timestamp,
    mensagem : criptografaDado(jsonObject.Dado),
    timestampFinal:'',
    tempoTotal:''
  }

  console.log(data)
  
  var options = {
    host: "coutmaster.ml",
    port: 443,
    path: "/dado/save",
    method: "POST",
    headers: headers
  };

  let erroAutenticacao = {
    errors:[
      'Failed to authenticate token.'
    ]
  }

  
  
  var https = require('https');
  var req = https.request(options, function(res) {  
    res.on('data', function(data) {
      console.log("Response:");
      console.log(data.toString())

      if(data.toString() == "Failed to authenticate token."){
        getTokenJwt(jsonObject.DeviceId,mensagem)
      }

      // if(equals(JSON.parse(data),erroAutenticacao))
      // {
      //   console.log("ERRRROU")
      // }
      // console.log(JSON.parse(data).errors[0])
      // if(JSON.parse(data).errors[0] == 'Failed to authenticate token.'){
      
      //   console.log('Token Falhou')
      //   getTokenJwt(jsonObject.DeviceId)
      // }
    });
  });
  
  req.on('error', function(e) {
    console.log("ERROR:");
    console.log(e);
  });
  
  req.write(JSON.stringify(data));
  req.end();
};


let getTokenJwt = function(deviceRecebido, mensagem) {
  let headers = {
    "Content-Type": "application/json",
    //"Authorization": token
  };

  const data = JSON.stringify({
    key:'12345',
    gatewayId:'03da950013be11ebab5874d4359afc5a'
  })
  
  var options = {
    host: "coutmaster.ml",
    port: 443,
    path: "/gateway/autenticate",
    method: "POST",
    headers: headers
  };
  
  var https = require('https');
  var req = https.request(options, function(res) {  
    res.on('data', function(data) {
      console.log("Response:");
      //console.log(JSON.parse(data));
      tokenNuvem = JSON.parse(data).token
      console.log(tokenNuvem)
      console.log(deviceRecebido)

    Token.findOne({device:deviceRecebido},(err,token)=>{
      if(err){
        console.log("Erro na busca")
      }else if(token){
        console.log(token._id)
        Token.update({_id:token._id},{$set:{token:tokenNuvem}},(err,resp)=>{
          if(err){
                console.log("Erro ao substituir")
              }else if(resp){
                console.log(resp)
                sendNotification(mensagem,tokenNuvem)
              }  
        })
          // Token.replaceOne({token:token.token},
          //   {token:tokenNuvem},(err,resp)=>{
          //   if(err){
          //     console.log("Erro ao substituir")
          //   }else if(resp){
          //     console.log(resp)
          //   }  
          // })
      }
      else{
        const newToken = new Token({ device: deviceRecebido,token: tokenNuvem })
         //console.log(newToken.token)
        newToken.save(err => {
              if(err) {
                  console.log("ERRO ao Salvar")
              } else {
                  console.log(newToken)
                  sendNotification(mensagem,tokenNuvem)
                  }
              })
      }
  
    })     
    });
  });
  
  req.on('error', function(e) {
    console.log("ERROR:");
    console.log(e);
  });
  
  req.write(data);
  req.end();
};

//getTokenJwt()

// let client = mqtt.connect('mqtt://coutmaster.ddns.net')
let client = mqtt.connect('mqtt://broker.hivemq.com')

client.on('connect', function(){
    client.subscribe(topico_subscribe, function(err){
        if(!err){
            console.log("Conectado " + topico_subscribe)
        }else{
            console.log("Erro")
        }
    })

    client.subscribe(topico_subscribe_latencia, function(err){
      if(!err){
          console.log("Conectado " + topico_subscribe_latencia )
      }else{
          console.log("Erro")
      }
  })

  client.subscribe(topico_subscribe_teste, function(err){
    if(!err){
        console.log("Conectado " + topico_subscribe_teste )
        client.publish("teste/teste/teste","teste")
    }else{
        console.log("Erro")
    }
})

  //   client.subscribe('AutDevice', function(err){
  //     if(!err){
  //         console.log("Conectado")
  //     }else{
  //         console.log("Erro")
  //     }
  // })
})


client.on('message', function(topic,message, packet){
   
   if(topic == topico_subscribe){
    //console.log(message.toString())
    let deviceId = JSON.parse(message).DeviceId

    Token.findOne({device:deviceId},(err, token) => {
              if(err) {
                  return console.log("ERRO 1")
              } else if (token) {
                  sendNotification(message,token.token)
                  //return console.log(token.token)
              } else {
                  getTokenJwt(deviceId,message)    
              }
         })
        


    //console.log(device.Nome)
   }

   else if(topic == topico_subscribe_latencia)
   {
    let jsonRecebido = JSON.parse(message)

    let timestampFinal = moment().valueOf()
    let tempoTotal = ((timestampFinal.valueOf())-(jsonRecebido.timestampInicial.valueOf()))

    const newDado = new Dado({
      codigo:jsonRecebido.codigo,
      timestampInicial : jsonRecebido.timestampInicial,
      timestampFinal : timestampFinal,
      tempoTotal : tempoTotal.valueOf(),
      deviceId : jsonRecebido.deviceId,
      mensagem : jsonRecebido.mensagem
  })


    newDado.save(err => {
       if(err) {
            console.log("ERRO ao Salvar")
       } else {
          console.log(newDado)
       }
    })

     console.log("Publicou " + newDado)
   }

   else if(topic == topico_subscribe_teste)
   {
    // console.log(message.toString())
    // let deviceId = JSON.parse(message).teste
    console.log(packet)
    console.log(packet.payload.length)
    // console.log(client)

    makeRnaPost()
   }

  //  if(topic == topico_subscribe){
  //     console.log(topic.toString())
  //     let separador = message.toString().split('#')
  //     let dado = separador[0]
  //     let token = separador[1]
  //     let mensagemPost = {
  //       mensagem:dado
  //     }
  //     sendNotification(mensagemPost,token)
  //   }else if(topic == 'AutDevice'){
  //     console.log(topic.toString())
  //     let separador2 =  message.toString().split('#')
  //     let name = separador2[0]
  //     let deviceId = separador2[1]

  //     Device.findOne({name},(err, device) => {
  //         if(err) {
  //               client.publish('AutDeviceReturn',"ERRO")
  //         } else if (device && bcrypt.compareSync(deviceId, device.deviceId)) {
  //               const token = jwt.sign(device, env.authSecret, {
  //               expiresIn: "1 day"})
  //               client.publish('AutDeviceReturn',token)
  //         } else {
  //           client.publish('AutDeviceReturn',"Nome ou ID ERRADOS !!")
  //        }
  //    })
  //   }

})

let makeRnaPost = ()=>{
  let jsonObject = {
    tcp_flags:"0x00000018",
    tcp_len:"51",
    mqtt_msg:"42.43",
    mqtt_conflag_qos:"0",
    mqtt_len:"27",
    mqtt_proto_len:"4",
    mqtt_qos:"0",
    mqtt_retain:"0",
    mqtt_ver:"3",
    mqtt_sub_qos:"0",
    mqtt_conflag_passwd:"1",
    mqtt_conflag_retain:"0",
    mqtt_conflag_uname:"1",
    mqtt_conflag_willflag:"0",
    mqtt_suback_qos:"0",
    mqtt_willmsg:"0",
    mqtt_willmsg_len:"15",
    mqtt_willtopic:"1",
    mqtt_willtopic_len:"8"
  } 

  let headers = {
    "Content-Type": "application/json",
    //"Authorization": token
  };

  let optionsRna = {
    host: "172.29.0.4",
    port: 5000,
    path: "/predict",
    method: "POST",
    headers: headers
  };


  var http = require('http');
  var req = http.request(optionsRna, function(res) {  
    res.on('data', function(data) {
      console.log("Response:");
      console.log(data.toString())
    });
  });
  
  req.on('error', function(e) {
    console.log("ERROR:");
    console.log(e);
  });
  
  req.write(JSON.stringify(jsonObject));
  req.end();
}