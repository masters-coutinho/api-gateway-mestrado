const lodash = require('lodash')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const Device = require('../Device/device')
const env = require('../Config/constants')

const emailRegex = /\S+@\S+\.\S+/
//const passwordRegex = /((?=.\d)(?=.[a-z])(?=.[A-Z])(?=.[@#$%]).{6,12})/
const passwordRegex = /((?=.\d)(?=.[a-z])(?=.*[A-Z]).{6,12})/

const sendErrorsFromDB = (res, dbErrors) => {
  const errors = []
  lodash.forIn(dbErrors.errors, error => errors.push(error.message))
  return res.status(400).json({errors})
}

const login = (req, res, next) => {
      const key = req.body.key || ''
      const deviceId =  req.body.deviceId || ''

    Device.findOne({deviceId},(err, device) => {
          if(err) {
                return sendErrorsFromDB(res, err)
          } else if (device && bcrypt.compareSync(key, device.key)) {
                const token = jwt.sign(device, env.authSecret, {
                expiresIn: "1 day"})
              //const { name, email } = user
              res.json({token})
          } else {
           return res.status(400).send({errors: ['Credenciais inválidas']})
         }
     })
    }

    const validateToken = (req, res, next) => {
          const token = req.body.token || ''
          jwt.verify(token, env.authSecret, function(err, decoded) {
            return res.status(200).send({valid: !err})
        })
    }

    const signup = (req, res, next) => {
        const key = req.body.key || ''
        const deviceId =  req.body.deviceId || ''

        const salt = bcrypt.genSaltSync()
        const keyHash = bcrypt.hashSync(key, salt)

        Device.findOne({deviceId}, (err, device) => {
            
             if(err) {
                return sendErrorsFromDB(res, err)
              } else if (device) {
                return res.status(400).send({errors: ['Nome do dispositivo já cadastrado.']})
              } else {
                const newDevice = new Device({ deviceId: deviceId ,key: keyHash })
                newDevice.save(err => {
                if(err) {
                    return sendErrorsFromDB(res, err)
                } else {
                       return res.status(200).send(newDevice)
                    }
                })
              }
        })
}



module.exports = { login, signup, validateToken }